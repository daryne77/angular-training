const express = require('express')
const bodyParser = require('body-parser');
const uuid = require('uuid');
const cors = require('cors')
const app = express()
const port = 3000

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

const tasks = [{id: uuid.v4(), title: 'Homework', done: false}, {id: uuid.v4(), title: 'Clean kitchen', done: false}, {id: uuid.v4(), title: 'Feed the dog', done: false}];

app.get('/tasks', (req, res) => {
    res.json(tasks);
})

app.post('/tasks', (req, res) => {
    tasks.push({...req.body, id: uuid.v4()});
    res.json(tasks);
})

app.patch('/tasks', (req, res) => {
    const task = tasks.find(t => t.id === req.body.id);
    Object.assign(task, req.body);
    res.json(tasks);
})

app.delete('/tasks/:id', (req, res) => {
    const idx = tasks.findIndex(t => t.id === req.params.id);
    tasks.splice(idx, 1);
    res.json(tasks);
})

app.get('/tasks/long', (req, res) => {
    setTimeout(() => res.json(tasks), 2000);
})

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`)
})

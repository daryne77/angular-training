import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-long-screen',
    templateUrl: './long-screen.component.html',
    styleUrls: ['./long-screen.component.scss']
})
export class LongScreenComponent implements OnInit {
    public tasks = [{title:1}];

    constructor() {
    }

    ngOnInit(): void {
    }

}

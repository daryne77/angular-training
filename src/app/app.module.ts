import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TasksScreenComponent} from './tasks-screen/tasks-screen.component';
import { LongScreenComponent } from './long-screen/long-screen.component';

@NgModule({
    declarations: [
        AppComponent,
        TasksScreenComponent,
        LongScreenComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}

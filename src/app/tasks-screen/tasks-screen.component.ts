import {Component} from '@angular/core';

@Component({
    selector: 'app-tasks-screen',
    templateUrl: './tasks-screen.component.html',
    styleUrls: ['./tasks-screen.component.scss']
})
export class TasksScreenComponent {
    public tasks: any[] = [];

    constructor() {
        this.tasks = [
            {title: 'Homework', done: false}, {title: 'Clean kitchen', done: false}, {title: 'Feed the dog', done: false}
        ];
    }
}

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LongScreenComponent} from './long-screen/long-screen.component';
import {TasksScreenComponent} from './tasks-screen/tasks-screen.component';

const routes: Routes = [
    {
        path: 'long',
        component: LongScreenComponent,
    },
    {
        path: '',
        component: TasksScreenComponent,
    }];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
